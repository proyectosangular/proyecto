import { Component, OnInit } from '@angular/core';
import { faUserLock } from '@fortawesome/free-solid-svg-icons';
import {FormGroup, FormControl} from '@angular/forms'
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-iniciar',
  templateUrl: './iniciar.component.html',
  styleUrls: ['./iniciar.component.css'],
  providers:[AuthService]
})
export class IniciarComponent implements OnInit {

  faUserLock = faUserLock;

  constructor() { }

  ngOnInit(): void {
  }

  formIniciar = new FormGroup({
    email: new FormControl(''),
    pass: new FormControl(''),

  })

  ingresar(){
    console.log('Form->', this.formIniciar.value);
  }

}
