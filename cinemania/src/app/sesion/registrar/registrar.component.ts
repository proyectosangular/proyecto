import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css'],
})
export class RegistrarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  formRegistro = new FormGroup({
    email: new FormControl(''),
    pass: new FormControl(''),
    nombre: new FormControl(''),
    ap: new FormControl(''),
    numero: new FormControl(''),
    cvv: new FormControl(''),
    mes: new FormControl(''),
    an: new FormControl(''),
    plan: new FormControl(''),

  });

registrar(){
  console.log('Form->', this.formRegistro.value); 
}

}
