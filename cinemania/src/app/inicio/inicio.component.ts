import { Component, OnInit } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  title = 'cinemania';
  faCheck = faCheck;
  faTimes = faTimes;
  faAngleRight = faAngleRight;

  constructor(private router:Router){} 

  ngOnInit(): void {
  }
  
  login() {
    this.router.navigate(['/iniciar']);
  }

  descubre() {
    this.router.navigate(['/peliculas']);
  }

  registro() {
    this.router.navigate(['/registro']);
  }

}
