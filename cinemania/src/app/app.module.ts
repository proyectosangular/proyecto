import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { InicioComponent } from './inicio/inicio.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { ReactiveFormsModule } from '@angular/forms';
import { IniciarComponent } from './sesion/iniciar/iniciar.component';
import { RegistrarComponent } from './sesion/registrar/registrar.component';


@NgModule({
  declarations: [
    AppComponent,
    IniciarComponent,
    RegistrarComponent,
    InicioComponent,
    PeliculasComponent,    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ReactiveFormsModule,

  ],
  providers: [],
  bootstrap: [AppComponent, IniciarComponent, InicioComponent, RegistrarComponent]
})
export class AppModule { }