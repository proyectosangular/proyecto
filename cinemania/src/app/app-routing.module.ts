import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { IniciarComponent } from './sesion/iniciar/iniciar.component';
import { RegistrarComponent } from './sesion/registrar/registrar.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: "/inicio",
    pathMatch: 'full'
  },
  {
    path: 'iniciar',
    component: IniciarComponent
  },
  {
    path: 'inicio',
    component: InicioComponent
  },
  {
    path: 'peliculas',
    component: PeliculasComponent
  },
  {
    path: 'registro',
    component: RegistrarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
